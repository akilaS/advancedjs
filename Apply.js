var foo = (function () {
    function hello(who) {
        return "Let me introduce " + who;
    }

    return {
        hello: hello
    }
})();


var bar = function (foo) {
    var name = 'Akila';

    console.log(foo.hello(name).toUpperCase());

};

bar.call(bar, foo);
