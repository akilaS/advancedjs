var o1 = {
    bar: 'bar1',
    foo: function () {
        console.log(this.bar);
    }
}

var o2 = {
    bar: 'bar2',
    foo: o1.foo,
    x: function () {
        console.log(this.bar);
    }
}

var bar = 'bar3';
var foo = o1.foo;

console.log(o1.foo());
console.log(o2.foo());
console.log(o2.x());
console.log(foo());



/*function foo() {
    var bar = 'bar';
    console.log(this.bar);
    baz();
}

function baz() {
    console.log(this.bar);
}

var bar = 'bar2';
console.log(foo());*/
