/** 
    Closure is when a FUNCTION "remembers" its lexical scope 
    even when the FUNCTION is executed outside of the lexical scope
*/


/*function foo() {
    var bar = 'bar';

    function baz() {
        console.log(bar);
    }

    bam(baz);
}

function bam(baz) {
    baz();
}

foo();*/

// Variables can be accessed via closures disregard of
// how deep the fucntions lie in the scope.

/*function foo() {
    var bar = 0;

    setTimeout(function () {
        var baz = 1;
        console.log(++bar + ' - foo');

        setTimeout(function () {
            console.log(++bar + ' - foo');
            console.log(bar + baz);
        }, 100);

    }, 200);
}*/

// Scope are not garbage collected untill all the function referenced 
// via closures are disallocated.

/*function bar() {
    var bar = 0;

    setTimeout(function () {
        console.log(++bar + ' - bar');
    }, 100);

    setTimeout(function () {
        console.log(++bar + ' - bar');
    }, 200);
}

foo();
bar();*/

for (var i = 0; i < 5; i++) {
    (function (i) {
        setTimeout(function () {
            console.log('i: ' + i);
        }, i * 1000);
    })(i);
}

/*for (let i = 0; i < 5; i++) {
    setTimeout(function () {
        console.log('j: ' + i);
    }, i * 1000);
}

for (var i = 0; i < 5; i++) {
    let j = i;
    setTimeout(function () {
        console.log('j: ' + j);
    }, j * 1000);
}*/


// Classic module pattern using closures

/**
    1. There should be a outer function executing around - IIFE or some wrapper
    2. There should be one or more functions returing from the 
       function call that has closure over the private scope of the function
*/

/*var foo = (function () {
    var o = {
        bar: 'bar'
    };

    return {
        bar: function () {
            console.log(o.bar);
        }
    }
})();

foo.bar();*/
