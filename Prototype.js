// Object Orienting in javascript

/**
    1. Every single object is constructed by a constructor function
    2. Every time a constructor is called a new object is created
    3. A constructor makes an object linked to its own prototype
*/

/*function Foo(who) {
    this.me = who;
}

Foo.prototype.identify = function () {
    return 'I am ' + this.me;
}

var a1 = new Foo('a1');
var a2 = new Foo('a2');

a2.speak = function () {
    alert('Hello, ' + this.identify());
}

console.log(a1.constructor === Foo);
console.log(a1.constructor === a2.constructor);
console.log(a1.__proto__ === Foo.prototype);
console.log(a1.__proto__ === a2.__proto__);
console.log(a1.__proto__ === Object.getPrototypeOf(a1));
console.log(a2.constructor === Foo);
console.log(a2.__proto__ === a2.constructor.prototype);*/


/*function Foo(who) {
    this.me = who;
}

Foo.prototype.identify = function () {
    return 'I am ' + this.me;
}

var a1 = new Foo('a1');
alert(a1.identify());

a1.identify = function () {
    alert('Hello ' + Foo.prototype.identify.call(this));
}

a1.identify();*/

function Foo(who) {
    this.me = who;
}

Foo.prototype.identify = function () {
    return 'I am ' + this.me;
}

function Bar(who) {
    //this.me = who;
    Foo.call(this, who);
}

Bar.prototype = Object.create(Foo.prototype);

Bar.prototype.speak = function () {
    alert('Hello, ' + this.identify());
}

var b1 = new Bar('b1');
var b2 = new Bar('b2');

b1.speak();
b2.speak();
