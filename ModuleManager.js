var ModuleManager = (function () {
    var modules = {};

    function define(name, deps, impl) {
        for (var i = 0; i < deps.length; i++) {
            deps[i] = modules[deps[i]];
        }
        modules[name] = impl.apply(impl, deps);
    }

    function get(name) {
        return modules[name];
    }

    return {
        define: define,
        get: get
    };

})();

ModuleManager.define("foo", [], function x() {
    function hello(who) {
        return "Let me introduce " + who;
    }

    return {
        hello: hello
    }
});

ModuleManager.define("bar", ["foo"], function y(foo) {
    var name = 'Akila';

    function awesome() {
        console.log(foo.hello(name).toUpperCase());
    }

    return {
        awesome: awesome
    }
});

var foo = ModuleManager.get("foo");
var bar = ModuleManager.get("bar");

console.log(foo.hello("akila"));
bar.awesome();
